//
//  Photo.swift
//  Bond And Accio Tutorial
//
//  Created by Vo The Dong An on 20/2/2020.
//  Copyright © 2020 vtdan. All rights reserved.
//

import Foundation

// Represents a single photo as returned by the 500px API
struct Photo: Codable {
    let title: String
    let url: URL
    let date: Date
}

//
//  PhotoQuery.swift
//  Bond And Accio Tutorial
//
//  Created by Vo The Dong An on 20/2/2020.
//  Copyright © 2020 vtdan. All rights reserved.
//

import Foundation

// Represents a query that can be used to retrieve photos from the 500px API
struct PhotoQuery: Codable {
    var text = ""
    var creativeCommonsLicence = false
    var dateFilter = false
    var maxDate = Date()
    var minDate = Date()
}

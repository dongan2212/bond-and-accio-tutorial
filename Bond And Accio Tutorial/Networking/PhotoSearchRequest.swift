//
//  PhotoSearchRequest.swift
//  Bond And Accio Tutorial
//
//  Created by Vo The Dong An on 24/2/2020.
//  Copyright © 2020 vtdan. All rights reserved.
//

import Foundation

public class PhotoSearchRequest {
    
    var consumerKey: String?
    var imageSize: Int?
    var term: String?
    var licenseType: String?
    
    public init(consumerKey: String? = nil,
                 imageSize: Int? = nil,
                 term: String? = nil,
                 licenseType: String? = nil) {
        self.consumerKey = consumerKey
        self.imageSize = imageSize
        self.term = term
        self.licenseType = licenseType
    }
}

//
//  APIClient.swift
//  Bond And Accio Tutorial
//
//  Created by Vo The Dong An on 24/2/2020.
//  Copyright © 2020 vtdan. All rights reserved.
//

import Foundation


import Foundation

typealias Photos = [Photo]

protocol APIClient {
    var apiKey: String? { get }
    var baseUrl: String { get }
}

extension APIClient {
    
}

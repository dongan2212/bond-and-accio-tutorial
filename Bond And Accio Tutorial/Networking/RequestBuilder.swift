//
//  RequestBuilder.swift
//  Bond And Accio Tutorial
//
//  Created by Vo The Dong An on 24/2/2020.
//  Copyright © 2020 vtdan. All rights reserved.
//

import Foundation

public class PhotoSearchRequestBuilder {
    
    var consumerKey: String?
    var imageSize: Int?
    var term: String?
    var licenseType: String?
    
    public init() { }
    
    @discardableResult func with(consumerKey: String) -> PhotoSearchRequestBuilder {
        self.consumerKey = consumerKey
        return self
    }
    
    @discardableResult func with(imageSize: Int) -> PhotoSearchRequestBuilder {
        self.imageSize = imageSize
        return self
    }
    
    @discardableResult func with(term: String) -> PhotoSearchRequestBuilder {
        self.term = term
        return self
    }
    
    @discardableResult func with(licenseType: String) -> PhotoSearchRequestBuilder {
        self.licenseType = licenseType
        return self
    }
    
    public func build() -> PhotoSearchRequest {
        return PhotoSearchRequest(consumerKey: consumerKey,
                                  imageSize: imageSize,
                                  term: term,
                                  licenseType: licenseType)
    }
}


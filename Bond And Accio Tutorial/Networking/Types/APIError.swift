//
//  APIError.swift
//  Bond And Accio Tutorial
//
//  Created by Vo The Dong An on 20/2/2020.
//  Copyright © 2020 vtdan. All rights reserved.
//

import Foundation

enum APIError: Error {
    case malformedRequest
    case requestFailed
    case invalidResponse
}

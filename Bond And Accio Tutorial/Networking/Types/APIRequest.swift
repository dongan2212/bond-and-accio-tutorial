//
//  APIRequest.swift
//  Bond And Accio Tutorial
//
//  Created by Vo The Dong An on 24/2/2020.
//  Copyright © 2020 vtdan. All rights reserved.
//

import Foundation

import Foundation

enum APIHost: Hashable {
    
    case photo
    
    var host: String {
        switch self {
        case .photo:
            return "api.500px.com"
//        default:
//            return ""
        }
    }
}

enum APIEndpoint: Hashable {
    
    case photo
    
    var path: String {
        switch self {
        case .photo:
            return "/v1/photos/search"
//        default:
//            return ""
        }
    }
}

struct APIParams {
    
}

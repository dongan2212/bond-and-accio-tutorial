//
//  APIResult.swift
//  Bond And Accio Tutorial
//
//  Created by Vo The Dong An on 20/2/2020.
//  Copyright © 2020 vtdan. All rights reserved.
//

import Foundation

public enum Result<T> {
    case success(T)
    case failure(Error)
}

extension Result {
    
    public var value: T? {
        switch self {
        case .success(let value):
            return value
        case .failure:
            return nil
        }
    }
    
    public var error: Error? {
        switch self {
        case .success:
            return nil
        case .failure(let error):
            return error
        }
    }
}

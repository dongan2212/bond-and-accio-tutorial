//
//  PhotoTableViewCell.swift
//  Bond And Accio Tutorial
//
//  Created by Vo The Dong An on 20/2/2020.
//  Copyright © 2020 vtdan. All rights reserved.
//

import UIKit

class PhotoTableViewCell: UITableViewCell {

    @IBOutlet private weak var photoImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

//
//  PhotoSearchViewController.swift
//  Bond And Accio Tutorial
//
//  Created by Vo The Dong An on 20/2/2020.
//  Copyright © 2020 vtdan. All rights reserved.
//

import UIKit
import Bond

class PhotoSearchViewController: UIViewController {

    @IBOutlet private weak var searchTextField: UITextField!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var resultTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addBondObservers()
    }
    
    func addBondObservers() {
        addObserverUppercasingSearchTextFieldText()
        addObserverDisplayAndAnimationIndicator()
    }
    
    func addObserverUppercasingSearchTextFieldText() {
        _ = self.searchTextField.reactive.text
            .map { $0?.uppercased() }
            .observeNext { text in
                guard let text = text else { return }
                print("-> observeNext: \(text)") }
    }
    
    func addObserverDisplayAndAnimationIndicator() {
        self.searchTextField.reactive.text.map { ($0?.count ?? 0) == 0 }.bind(to: activityIndicator.reactive.isHidden)
        self.searchTextField.reactive.text.map { ($0?.count ?? 0) > 0 }.bind(to: activityIndicator.reactive.isAnimating)
    }
}

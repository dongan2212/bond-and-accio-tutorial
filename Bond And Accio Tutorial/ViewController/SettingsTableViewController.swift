//
//  SettingsTableViewController.swift
//  Bond And Accio Tutorial
//
//  Created by Vo The Dong An on 20/2/2020.
//  Copyright © 2020 vtdan. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {

    @IBOutlet private weak var creativeCommonsSwitch: UISwitch!
    @IBOutlet private weak var filterDatesSwitch: UISwitch!
    @IBOutlet private weak var maxDatePickerCell: DatePickerTableViewCell!
    @IBOutlet private weak var minDatePickerCell: DatePickerTableViewCell!
    private let defaultRowHeight: CGFloat = 44.0

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
}

// MARK: - Private functions
private extension SettingsTableViewController {

    func configureUI() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = defaultRowHeight
        maxDatePickerCell.leftLabel.text = "Max Date"
        minDatePickerCell.leftLabel.text = "Min Date"
    }
}

// MARK: - Table view data source & delegate
extension SettingsTableViewController {

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("--> \(#function)")
        guard let datePickerCell = tableView.cellForRow(at: indexPath) as? DatePickerTableViewCell else { return }
        datePickerCell.selectedInTableView(tableView)
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }


    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        print("--> \(#function)")
        guard let selected = tableView.indexPathForSelectedRow,
            selected == indexPath,
            let datePickerCell = tableView.visibleCells[selected.row] as? DatePickerTableViewCell else { return defaultRowHeight }
        return datePickerCell.datePickerHeight()
    }
}

// MARK:  - @IBAction
extension SettingsTableViewController {

    @IBAction func onDismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

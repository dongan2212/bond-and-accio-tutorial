// swift-tools-version:5.0
import PackageDescription

let package = Package(
    name: "Bond And Accio Tutorial",
    products: [],
    dependencies: [
        // Elegant HTTP Networking in Swift
        .package(url: "https://github.com/Alamofire/Alamofire.git", .exact("4.9.0")),
        
        // Swift binding framework that takes binding concepts to a whole new level
        .package(url: "https://github.com/DeclarativeHub/Bond", .branch("master")),
                    
        // The Swift (and Objective-C) testing framework.
        .package(url: "https://github.com/Quick/Quick", .upToNextMajor(from: "2.1.0")),
        .package(url: "https://github.com/Quick/Nimble", .upToNextMinor(from: "8.0.3"))
    ],
    targets: [
        .target(
            name: "Bond And Accio Tutorial",
            dependencies: [
                "Alamofire",
                "Bond"
            ],
            path: "Bond And Accio Tutorial"
        ),
        .testTarget(
            name: "Bond And Accio TutorialTests",
            dependencies: [
                "Quick",
                "Nimble"
            ],
            path: "Bond And Accio TutorialTests"
        ),
    ]
)
